# Ejercicio To Do List



## Ejercicio React

Aplicación permite crear una lista de items/actividades del tipo To DO que permite eliminarlos según se vayan realizando.

```
http://3.144.197.204/todo/
```


## Instalación en local

Proyecto esta creado utilizando NPM por lo cual es requisito para su instalación.
Necesitarás tener Node >= 14.0.0 y npm >= 5.6 instalados en tu máquina.

Descargamos el proyecto, compilamos y desplegamos.

```
$ git clone https://gitlab.com/brjgd/todolist.git
$ cd todolist
$ npm install
$ npm start
```

## Arquitectura de solución

El desarrollo esta utilizando lo que esta proponiendo React para el manejo de componentes y estados bajo Hooks.
Los hooks simplifican el manejo de componentes frente al uso de clases para la creación de estos, la reutilización de manera mas simple, mejorando la sintaxis e incluso lectura de código.




