import React, { useState } from "react";
 
const ToDoList = () => {
  const [list, setList] = useState([]);
  const [item, setItem] = useState({
    element: "",
  });
 
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setItem({
      ...item,
      [name]: value
    });
  };
  const borrar = (index) => {
    setList((currentList) => currentList.filter((item) => item.id !== index));
  };
  const handleSubmit = (event) => {
    
    event.preventDefault();
    const { element } = item;
    if (element) {
       setList((currentList) => [
        ...currentList,
        { element, id: currentList.length }
      ]);
      setItem({ element: ""});
    } else {
        alert("Por favor completa el campo");
    }
  };
  return (
    <>
      <div className="input">
        <form onSubmit={handleSubmit} className="form">
          <Input
            type="text"
            label="Item"
            name="element"
            value={item.element}
            handleInput={handleInputChange}
          />
          <div className="pax_btn">
            <button type="submit" className="btn btn-primary">
              Agregar
            </button>
          </div>
        </form>
      </div>
      <div className="table_list">
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">To Do:</th>
            </tr>
          </thead>
          <tbody>
            {Array.isArray(list) &&
              list.length > 0 &&
              list.map((item) => (
                <TableItem key={item.id} borrar={borrar} {...item} />
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default ToDoList;
 
const Input = ({ name, value, handleInput, label, type, ...props }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>
        {label}
        <input
          type={type}
          className="form-control"
          id={name}
          name={name}
          value={value}
          onChange={handleInput}
          {...props}
        />
      </label>
    </div>
  );
};
 
const TableItem = ({ id, element, borrar }) => {
  return (
    <tr>
      <td>{element}</td>
      <td>
        <button onClick={() => borrar(id)}>borrar</button>
      </td>
    </tr>
  );
};